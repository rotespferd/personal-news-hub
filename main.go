package main

import (
	"crypto/sha512"
	"fmt"

	"github.com/mmcdole/gofeed"
	"rsc.io/quote"
)

func main() {
	banner := " _____  _   _ _    _\n" +
		"|  __ \\| \\ | | |  | |\n" +
		"| |__) |  \\| | |__| |\n" +
		"|  ___/| . ` |  __  |\n" +
		"| |    | |\\  | |  | |\n" +
		"|_|    |_| \\_|_|  |_|"

	fmt.Println(banner)
	fmt.Println(quote.Go())

	fp := gofeed.NewParser()
	feed, _ := fp.ParseURL("https://www1.wdr.de/nachrichten/westfalen-lippe/uebersicht-westfalen-lippe-100.feed")
	fmt.Println(feed.Custom)

	hash := sha512.New()
	hash.Write([]byte(feed.String()))
	fmt.Printf("%x\n", hash.Sum(nil))
}
